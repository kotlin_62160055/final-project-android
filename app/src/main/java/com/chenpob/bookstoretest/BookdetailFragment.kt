package com.chenpob.bookstoretest

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.chenpob.bookstoretest.data.Book
import com.chenpob.bookstoretest.data.getFormattedPrice
import com.chenpob.bookstoretest.databinding.FragmentBookdetailBinding
import com.squareup.picasso.Picasso

class BookdetailFragment : Fragment() {

    private val viewModel: BookViewModel by activityViewModels {
        BookViewModelFactory(
            (activity?.application as BookApplication).database.bookDao()
        )
    }

    private val navigationArgs: BookdetailFragmentArgs by navArgs()

    private var _binding: FragmentBookdetailBinding? = null
    private val binding get() = _binding!!

    lateinit var book: Book

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBookdetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.bookId
        viewModel.retrieveBook(id).observe(this.viewLifecycleOwner) { selectedItem ->
            book = selectedItem
            binding.apply {
                bookNametext.text = book.bookName
                bookDetailText.text = book.bookDetail
                bookPriceText.text = book.getFormattedPrice() + " Baht"

                val Imgid = resources.getIdentifier(""+book.bookImg, "drawable",context?.packageName)
                imageBook.setImageResource(Imgid)

                cancelButton.setOnClickListener {
                    findNavController().navigateUp()
                }
                confirmButton.setOnClickListener {
                    val action = BookdetailFragmentDirections.actionBookdetailFragmentToPaymentFragment(book.id)
                    findNavController().navigate(action)
                }
            }
        }
    }

    companion object {

    }
}