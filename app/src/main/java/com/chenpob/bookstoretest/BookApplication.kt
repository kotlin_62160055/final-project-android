package com.chenpob.bookstoretest

import android.app.Application
import com.chenpob.bookstoretest.data.BookRoomDatabase

class BookApplication : Application() {
    val database: BookRoomDatabase by lazy{ BookRoomDatabase.getDatabase(this) }
}