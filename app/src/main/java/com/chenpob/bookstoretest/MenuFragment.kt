package com.chenpob.bookstoretest

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.chenpob.bookstoretest.databinding.FragmentMenuBinding
import com.squareup.picasso.Picasso
import java.util.concurrent.Executors

class MenuFragment : Fragment() {

    private var _binding: FragmentMenuBinding? = null
    private val binding get() = _binding!!

    private var imageView: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentMenuBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.StoreButton.setOnClickListener {
          val action = MenuFragmentDirections.actionMenuFragmentToStorebookFragment()
            findNavController().navigate(action)
        }

        binding.HistoryButton.setOnClickListener {
            val action = MenuFragmentDirections.actionMenuFragmentToHistoryFragment()
            findNavController().navigate(action)
        }
    }



    companion object {

    }
}