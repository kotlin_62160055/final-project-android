package com.chenpob.bookstoretest

import android.location.Address
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.chenpob.bookstoretest.data.Book
import com.chenpob.bookstoretest.data.Order
import com.chenpob.bookstoretest.data.getFormattedPrice
import com.chenpob.bookstoretest.databinding.FragmentPaymentBinding


class PaymentFragment : Fragment() {

    private val bookViewModel: BookViewModel by activityViewModels {
        BookViewModelFactory(
            (activity?.application as BookApplication).database.bookDao()
        )
    }

    private val orderViewModel: OrderViewModel by activityViewModels {
        OrderViewModelFactory(
            (activity?.application as BookApplication).database.orderDao()
        )
    }

    private var _binding: FragmentPaymentBinding? = null
    private val binding get() = _binding!!

    private val navigationArgs: PaymentFragmentArgs by navArgs()
    lateinit var book: Book
    lateinit var order: Order

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentPaymentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.bookId
        bookViewModel.retrieveBook(id).observe(this.viewLifecycleOwner) { selectedItem ->
            book = selectedItem
            binding.apply {

                showTotal()
                showAmount()

                plusButton.setOnClickListener {
                    amountEdt.text = orderViewModel.addAmount().toString()
                    showTotal()
                }

                minusButton.setOnClickListener {
                    amountEdt.text = orderViewModel.cutAmount().toString()
                    showTotal()
                }

                confirmButton.setOnClickListener {
                    if(orderViewModel.isEntryValid(
                            nameEdt.text.toString(),
                            addressEdt.text.toString())){
                        addOrder(nameEdt.text.toString()
                            ,addressEdt.text.toString()
                            ,book.id
                            ,orderViewModel.getAmount()
                            ,orderViewModel.getTotal()
                        )
                    }
                }

                cancelButton.setOnClickListener {
                    cancelOrder()
                }



            }
        }
    }

    private fun showTotal(){
        binding.sumpricetext.text = orderViewModel.getTotal(
            book.bookPrice,
            orderViewModel.getAmount()).toString() + " Baht"
    }

    private fun showAmount(){
        binding.amountEdt.text = orderViewModel.getAmount().toString()
    }

    private fun addOrder(orderName: String,orderAddress: String,orderBook:Int,amount:Int,total:Int){

//        Log.d("name",orderName)
//        Log.d("address",orderAddress)
//        Log.d("book id", ""+orderBook)
//        Log.d("amount",""+amount)
//        Log.d("total",""+total)

        orderViewModel.addNewOrder(orderName,orderAddress,orderBook,amount,total)

        orderViewModel.resetOrder()
        val action = PaymentFragmentDirections.actionPaymentFragmentToMenuFragment()
        findNavController().navigate(action)
    }

    private fun cancelOrder(){
        orderViewModel.resetOrder()
        findNavController().navigateUp()
    }

    companion object {

    }
}