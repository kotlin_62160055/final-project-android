package com.chenpob.bookstoretest

import androidx.lifecycle.*
import com.chenpob.bookstoretest.data.Order
import com.chenpob.bookstoretest.data.OrderDao
import kotlinx.coroutines.launch

class OrderViewModel(private val orderDao: OrderDao): ViewModel() {

    val allOrders:LiveData<List<Order>> = orderDao.getOrders().asLiveData()

    private var total = 0

    private var amount = 1

    private fun insertItem(order: Order) {
        viewModelScope.launch {
            orderDao.insert(order)
        }
    }

    fun retrieveOrder(id: Int): LiveData<Order> {
        return orderDao.getOrder(id).asLiveData()
    }

    private fun getNewOrderEntry(orderName:String,
                                 orderAddress:String,
                                 orderBook :Int,
                                 orderAmount:Int,
                                 orderTotal:Int): Order {
        return Order(
            orderName = orderName,
            orderAddress = orderAddress,
            orderBook = orderBook,
            orderAmount = orderAmount,
            orderTotal = orderTotal
        )
    }

    fun addNewOrder(orderName: String,orderAddress: String,orderBook: Int,amount: Int,orderTotal: Int) {
        val newItem = getNewOrderEntry(orderName,orderAddress,orderBook,amount,orderTotal)
        insertItem(newItem)
    }

    fun isEntryValid(orderName: String, orderAddress: String): Boolean {
        if (orderName.isBlank() || orderAddress.isBlank()) {
            return false
        }
        return true
    }

    fun addAmount(): Int{
        amount++
        return getAmount()
    }

    fun cutAmount(): Int{
        return if (amount <= 1){
            getAmount()
        }else{
            amount--
            getAmount()
        }
    }

    fun getAmount(): Int {
        return amount
    }

    private fun calculateTotal(bookPrice:Int,amount:Int) : Int{
        total = bookPrice*amount
        return total
    }

    fun getTotal(bookPrice:Int,amount:Int): Int {
        return (calculateTotal(bookPrice,amount))
    }

    fun getTotal():Int{
        return total
    }

    fun resetOrder(){
        total = 0
        amount = 1
    }

}

class OrderViewModelFactory(private val order: OrderDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(OrderViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return OrderViewModel(order) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}