package com.chenpob.bookstoretest

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.chenpob.bookstoretest.adapter.BookListAdapter
import com.chenpob.bookstoretest.data.Book
import com.chenpob.bookstoretest.databinding.BookCardBinding
import com.chenpob.bookstoretest.databinding.FragmentStorebookBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [StorebookFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class StorebookFragment : Fragment() {

    private val viewModel: BookViewModel by activityViewModels {
        BookViewModelFactory(
            (activity?.application as BookApplication).database.bookDao()
        )
    }

    private var _binding: FragmentStorebookBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentStorebookBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.bookListView.layoutManager = LinearLayoutManager(this.context)

        val adapter = BookListAdapter {
            val action = StorebookFragmentDirections.actionStorebookFragmentToBookdetailFragment(it.id)
            this.findNavController().navigate(action)
        }
        binding.bookListView.adapter = adapter
        viewModel.allBooks.observe(this.viewLifecycleOwner) {
           books -> books.let {
               adapter.submitList(it)
        }

        }

    }

    companion object {
    }
}