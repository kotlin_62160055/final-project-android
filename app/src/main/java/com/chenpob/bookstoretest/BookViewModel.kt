package com.chenpob.bookstoretest

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import com.chenpob.bookstoretest.data.Book
import com.chenpob.bookstoretest.data.BookDao

class BookViewModel(private val bookDao: BookDao) : ViewModel() {

    val allBooks:LiveData<List<Book>> = bookDao.getItems().asLiveData()

    fun retrieveBook(id: Int): LiveData<Book> {
        return bookDao.getItem(id).asLiveData()
    }

}

class BookViewModelFactory(private val bookDao: BookDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(BookViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return BookViewModel(bookDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}