package com.chenpob.bookstoretest.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.chenpob.bookstoretest.data.Order
import com.chenpob.bookstoretest.databinding.HistoryCardBinding


class HistoryListAdapter(private val onOrderClick: (Order) -> Unit): ListAdapter<Order, HistoryListAdapter.HistoryViewHolder>(DiffCallback) {

    companion object{
        private val DiffCallback = object : DiffUtil.ItemCallback<Order>() {
            override fun areItemsTheSame(oldOrder: Order, newOrder: Order): Boolean {
                return oldOrder.orderName == newOrder.orderName
            }

            override fun areContentsTheSame(oldOrder: Order, newOrder: Order): Boolean {
                return oldOrder == newOrder
            }
        }
    }

    class HistoryViewHolder(private val binding: HistoryCardBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(order: Order){
            binding.apply {
                numText.text = order.id.toString()
                nameText.text = order.orderName
                addressText.text = order.orderAddress
                amountText.text = order.orderAmount.toString()
                totalText.text = order.orderTotal.toString()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        val viewHolder = HistoryViewHolder(
            HistoryCardBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

        viewHolder.itemView.setOnClickListener {
            val position = viewHolder.adapterPosition
            onOrderClick(getItem(position))
        }

        return viewHolder
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        val current = getItem(position)
        holder.itemView.setOnClickListener {
            onOrderClick(current)
        }
        holder.bind(current)
    }
}