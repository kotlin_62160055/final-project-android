package com.chenpob.bookstoretest.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.chenpob.bookstoretest.R
import com.chenpob.bookstoretest.data.Book
import com.chenpob.bookstoretest.databinding.BookCardBinding
import com.squareup.picasso.Picasso


class BookListAdapter(private val onBookClick: (Book) -> Unit) : ListAdapter<Book, BookListAdapter.BookViewHolder>(DiffCallback){
    companion object{
        private val DiffCallback = object : DiffUtil.ItemCallback<Book>() {
            override fun areItemsTheSame(oldBook: Book, newBook: Book): Boolean {
                return oldBook.bookName == newBook.bookName
            }

            override fun areContentsTheSame(oldBook: Book, newBook: Book): Boolean {
                return oldBook == newBook
            }
        }
    }
    class BookViewHolder(private var binding: BookCardBinding): RecyclerView.ViewHolder(binding.root){
        @SuppressLint("Format")
        fun bind(book: Book){
           binding.apply {
               bookName.text = book.bookName
               bookPrice.text = book.bookPrice.toString()

               val Imgid =  root.resources.getIdentifier(""+book.bookImg, "drawable",imgBook.context.packageName)
               imgBook.setImageResource(Imgid)

//               Picasso.get().load(R.drawable.bookemtry).fit().centerCrop()
//                   .placeholder(R.drawable.loading)
//                   .error(R.drawable.bookemtry)
//                   .into(imgBook)


//               imgBook.setImageResource(R.drawable.detective1)
           }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        val viewHolder = BookViewHolder(
            BookCardBinding.inflate(
                LayoutInflater.from( parent.context),
                parent,
                false
            )
        )
        viewHolder.itemView.setOnClickListener {
            val position = viewHolder.adapterPosition
            onBookClick(getItem(position))
        }

        return viewHolder
    }

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        val current = getItem(position)
        holder.itemView.setOnClickListener {
            onBookClick(current)
        }
        holder.bind(current)
    }
}