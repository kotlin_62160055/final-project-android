package com.chenpob.bookstoretest.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface OrderDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(order: Order)

    @Query("SELECT * from ordertable WHERE id = :id")
    fun getOrder(id: Int): Flow<Order>

    @Query("SELECT * from ordertable ORDER BY id DESC")
    fun getOrders(): Flow<List<Order>>

}