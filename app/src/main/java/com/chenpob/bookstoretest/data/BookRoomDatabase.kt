package com.chenpob.bookstoretest.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Book::class,Order::class], version = 1)
abstract class BookRoomDatabase : RoomDatabase() {
    abstract fun bookDao(): BookDao
    abstract fun orderDao(): OrderDao

    companion object {
        @Volatile
        private var INSTANCE: BookRoomDatabase? = null
        fun getDatabase(context: Context): BookRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context,
                    BookRoomDatabase::class.java,
                    "bookstore"
                )
                    .createFromAsset("database/bookstore.db")
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}