package com.chenpob.bookstoretest.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.RESTRICT
import androidx.room.PrimaryKey
import java.text.NumberFormat

@Entity(tableName = "booktable")
data class Book(
    @PrimaryKey
    val id: Int = 0,
    @ColumnInfo(name = "bookname")
    val bookName: String,
    @ColumnInfo(name = "bookdetail")
    val bookDetail: String,
    @ColumnInfo(name = "bookimg")
    val bookImg: String,
    @ColumnInfo(name = "bookprice")
    val bookPrice: Int,
)


@Entity(tableName = "ordertable",
        foreignKeys = [
            ForeignKey(entity = Book::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("order_book"),
            onDelete = RESTRICT,
            onUpdate = RESTRICT,
            )]
)
data class Order(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    @ColumnInfo(name = "order_name")
    val orderName: String,
    @ColumnInfo(name = "order_address")
    val orderAddress: String,
    @ColumnInfo(name = "order_book")
    val orderBook: Int,
    @ColumnInfo(name = "order_amount")
    val orderAmount: Int,
    @ColumnInfo(name = "order_total")
    val orderTotal: Int,
)

fun Book.getFormattedPrice(): String =
    NumberFormat.getCurrencyInstance().format(bookPrice)

