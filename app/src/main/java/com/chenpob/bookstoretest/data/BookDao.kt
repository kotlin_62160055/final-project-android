package com.chenpob.bookstoretest.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface BookDao {

    @Query("SELECT * from booktable WHERE id = :id")
    fun getItem(id: Int): Flow<Book>

    @Query("SELECT * from booktable ORDER BY bookname ASC")
    fun getItems(): Flow<List<Book>>
}