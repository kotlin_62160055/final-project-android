package com.chenpob.bookstoretest

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.chenpob.bookstoretest.adapter.HistoryListAdapter
import com.chenpob.bookstoretest.databinding.FragmentHistoryBinding

class HistoryFragment : Fragment() {

    private val orderViewModel: OrderViewModel by activityViewModels {
        OrderViewModelFactory(
            (activity?.application as BookApplication).database.orderDao()
        )
    }

    private var _binding: FragmentHistoryBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHistoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.historyList.layoutManager = LinearLayoutManager(this.context)

        val adapter = HistoryListAdapter{
        }

        binding.historyList.adapter = adapter
        orderViewModel.allOrders.observe(this.viewLifecycleOwner){
            orders -> orders.let {
                adapter.submitList(it)
        }
        }
    }

    companion object {

    }
}